package com.example.rodrigo.novidadelivronelsong.recycle_view

import android.view.View

interface ItemClickListener {
    fun onItemClick(view: View, position: Int, item: String)
}
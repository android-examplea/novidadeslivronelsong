package com.example.rodrigo.novidadelivronelsong.Parcelize

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.rodrigo.novidadelivronelsong.R
import kotlinx.android.synthetic.main.activity_tela2.*

class Tela2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tela2)


        var pessoa = intent.getParcelableExtra<Pessoa>("pessoa")
        textView2.text = "${pessoa.nome} - ${pessoa.idade}"
    }
}

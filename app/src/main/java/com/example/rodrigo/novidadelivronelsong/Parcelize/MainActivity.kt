package com.example.rodrigo.novidadelivronelsong.Parcelize

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.rodrigo.novidadelivronelsong.R

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // filtro de lista
        var month: List<String> = arrayListOf("January", "February", "March")
        var monthList = month.filter { s -> s == "January" }.single()
        Log.i("FILTER",monthList)
    }



    fun btTela2(v: View){
        startActivity(Intent(this@MainActivity, Tela2Activity::class.java)
            .putExtra("pessoa",Pessoa("rodrigo",24)))
    }

}

package com.example.rodrigo.novidadelivronelsong.recycle_view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.example.rodrigo.novidadelivronelsong.R
import kotlinx.android.synthetic.main.activity_recycle_view.*
import android.support.v4.widget.SwipeRefreshLayout






class RecycleViewActivity : AppCompatActivity(), ItemClickListener {


    var list: ArrayList<String>? = null
    var layoutManager: LinearLayoutManager? = null
    var adapterRecycle: AdapterRecycle? = null

    //paginacao
    var flag_loading = false
    var page = 0



    //TODO: expandir a célula



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycle_view)

        initRecycleView()
        serSwipeRefresh()
        setPagination()
        setSearchRecycleView()

    }



    fun initRecycleView(){
        list = ArrayList()
        for (i in 0 until 20){
            list?.add("item $i")
        }

        adapterRecycle = AdapterRecycle(this@RecycleViewActivity, list!!,this@RecycleViewActivity)
        recycleView.adapter = adapterRecycle
        layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
//        var layoutManager = GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false)
//        var layoutManager = StaggeredGridLayoutManager( 3, StaggeredGridLayoutManager.VERTICAL)
        recycleView.layoutManager = layoutManager
        swipeRefresh.isRefreshing = false
    }






//-------------------------------------------------PAGINAÇÃO------------------------------------------------------------

    fun setPagination(){
        recycleView.setOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val visibleItemCount = layoutManager?.getChildCount()
                val totalItemCount = layoutManager?.getItemCount()
                val firstVisibleItemPosition = layoutManager?.findFirstVisibleItemPosition()

                if(firstVisibleItemPosition!! + visibleItemCount!! == totalItemCount && totalItemCount!=0) {
                    if (flag_loading == false) {
                        flag_loading = true

                        page += 1
                        Log.i("PAGINACAO_INCRE","${page}")
                        setNewListForPaginaton()
                    }
                } else {
                    flag_loading = false
                }
            }
        })
    }

    fun setNewListForPaginaton(){
        var newList = ArrayList<String>()
        for (i in 20 until 41){
            newList.add("item $i")
        }
        adapterRecycle?.setListForPagination(newList)
    }





//---------------------------------------------------LISTENER DE CLICK--------------------------------------------------

    override fun onItemClick(view: View, position: Int, item: String) {
        Log.i("ITEM", item)
    }





//---------------------------------------------------BUSCA--------------------------------------------------------------

    fun setSearchRecycleView(){
        edtSearch.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                adapterRecycle?.filter?.filter(s.toString())
            }
        })
    }





//---------------------------------------------------SWIPE REFRESH------------------------------------------------------

    fun serSwipeRefresh(){
        swipeRefresh.setOnRefreshListener {
            initRecycleView()
        }
    }
}



























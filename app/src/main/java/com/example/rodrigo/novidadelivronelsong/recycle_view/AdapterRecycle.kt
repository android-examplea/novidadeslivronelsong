package com.example.rodrigo.novidadelivronelsong.recycle_view

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.example.rodrigo.novidadelivronelsong.R




class AdapterRecycle(var context: Context, var listItens: ArrayList<String>, var listener: ItemClickListener): RecyclerView.Adapter<AdapterRecycle.MyViewHolder>(), Filterable {



    var listOriginal: ArrayList<String> = listItens
    var listFiltered: ArrayList<String> = listItens
    var mFilter: ItemFilter = ItemFilter()
    var selectedPosition = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.adapter_recycleview, parent,false)
        var myViewHolder = MyViewHolder(view)
        return myViewHolder
    }



    override fun onBindViewHolder(viewHolder: MyViewHolder, position: Int) {
        var item = listFiltered[position]
        if(selectedPosition == position){
            viewHolder.itemView.setBackgroundColor(Color.GREEN)
        } else {
            viewHolder.itemView.setBackgroundColor(Color.parseColor("#ffffff"))
        }

        viewHolder.txtTitle.text = item
        viewHolder.itemView.setOnClickListener{
            listener.onItemClick(viewHolder.itemView, position, item)
            viewHolder.itemView.setBackgroundColor(Color.GREEN)

            selectedPosition = position
            notifyDataSetChanged()
        }


    }




    override fun getItemCount(): Int = listFiltered.size




    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        var txtMsg: TextView = itemView.findViewById(R.id.txtMsg)
    }











    //metodo de paginação
    fun setListForPagination(newList: ArrayList<String>){
        if (listOriginal.size == 0) {
            listOriginal = newList
            notifyDataSetChanged()
        } else {
            listOriginal.addAll(newList)
            notifyDataSetChanged()
        }
    }







    override fun getFilter(): Filter {
        return mFilter
    }



    inner class ItemFilter : Filter() {
        override fun performFiltering(constraint: CharSequence): Filter.FilterResults {

            val filterString = constraint.toString().toLowerCase()
            val results = Filter.FilterResults()
            val list = listOriginal

            val count = list.size
            var nlistt = ArrayList<String>(count)
            val listEquipments = ArrayList<String>()
            var newValue: String

            for (i in 0 until count) {
                val temp = list.get(i)
                newValue = temp
                var flagNameFantasy = newValue.toLowerCase().contains(filterString)

                if (flagNameFantasy) {
                    listEquipments.add(temp)
                }
            }

            nlistt = listEquipments
            results.values = nlistt
            results.count = nlistt.size
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            listFiltered = results.values as ArrayList<String>
            notifyDataSetChanged()
        }
    }


}